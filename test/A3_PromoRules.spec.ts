import PromoRules from '../src/lib/PromoRules'

describe('Clase PromoRules:', () => {
    let pr = new PromoRules({
        'PRUEBA1': 'DosPorUno',
        'PRUEBA2': 'DescuentoEnLaCompraDe3oMas',
        'PRUEBA3': 'PromocionNoValida'
    })
    console.log(pr)
    test('Deberia devolver un objeto con promocion DosPorUno', () => {
        let promo = pr.applyPromo({
            code: 'PRUEBA1',
            cantidad: 5,
            precio: 1,
            total: 5
        })
        expect(promo).toHaveProperty('totalPromo', 3)
        expect(promo).toHaveProperty('descuento', 2)
    })
})