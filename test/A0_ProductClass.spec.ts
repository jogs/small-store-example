import Product from '../src/lib/Product'

describe('Clase Product:', () => {
    let prod = new Product('PRUEBA', 'producto prueba', 10)
    test('Deberia incrementar la cantidad en de 1 a 2', () => {
        expect(prod.cantidad).toBe(1)
        prod.inc()
        expect(prod.cantidad).toBe(2)
    })
    test('Deberia devolver el un JSON', () => {
        expect(prod.toJSON()).toHaveProperty('code', 'PRUEBA')
        expect(prod.toJSON()).toHaveProperty('nombre', 'producto prueba')
        expect(prod.toJSON()).toHaveProperty('precio', 10)
        expect(prod.toJSON()).toHaveProperty('cantidad', 2)
        expect(prod.toJSON()).toHaveProperty('total', 20)
    })
    test('Deberia devolver un string', () => {
        expect(prod.toString()).toBe('PRUEBA: producto prueba | 2 x 10 = 20')
    })
})