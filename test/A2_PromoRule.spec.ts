import PromoRule from '../src/lib/PromoRule'

describe('Clase PromoRule', () => {
    describe('Promoción: DosPorUno', () => {
        let pr = new PromoRule('PRUEBA', 'DosPorUno')
        test('Deberia responder `DosPorUno`', () => {
            expect(pr.promoName).toBe('DosPorUno')
        })
        test('Deberia responder con 30', () => {
            expect(pr.promo(5, 10)).toBe(30)
        })
        test('Deberia responder con 30', () => {
            expect(pr.promo(6, 10)).toBe(30)
        })
        let pr2 = new PromoRule('PRUEBA', 'PromocionNoValida')
        test('Deberia responder con 50', () => {
            expect(pr2.promo(10, 5))
        })
    })
})