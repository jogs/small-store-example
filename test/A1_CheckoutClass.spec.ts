import Checkout from '../src/lib/Checkout'

describe('Clase Checkout:', () => {
    describe('Getters', () => {
        describe('Subtotal:', () => {
            let co = new Checkout()
            test('Deberia responder con 10', () => {
                co.scan('VOUCHER')
                co.scan('VOUCHER')
                expect(co.subtotal).toBe(10)
            })
            test('Deberia responder con 110', () => {
                co.scan('TSHIRT')
                co.scan('TSHIRT')
                co.scan('TSHIRT')
                co.scan('TSHIRT')
                co.scan('TSHIRT')
                expect(co.subtotal).toBe(110)
            })
            test('Deberia responder con 52.5', () => {
                co.scan('MUG')
                co.scan('MUG')
                co.scan('MUG')
                expect(co.subtotal).toBe(132.5)
            })
        })
        describe('Descuento:', () => {
            let co = new Checkout({
                'VOUCHER': 'DosPorUno',
                'TSHIRT': 'DescuentoEnLaCompraDe3oMas',
                'MUG': 'NA'
            })
            test('Deberia responder con 5', () => {
                co.scan('VOUCHER')
                co.scan('VOUCHER')
                expect(co.descuento).toBe(5)
            })
            test('Deberia responder con 100', () => {
                co.scan('TSHIRT')
                co.scan('TSHIRT')
                co.scan('TSHIRT')
                co.scan('TSHIRT')
                co.scan('TSHIRT')
                expect(co.descuento).toBe(10)
            })
            test('Deberia responder con 122.5', () => {
                co.scan('MUG')
                co.scan('MUG')
                co.scan('MUG')
                expect(co.descuento).toBe(10)
            })
        })
        describe('Total:', () => {
            let co = new Checkout({
                'VOUCHER': 'DosPorUno',
                'TSHIRT': 'DescuentoEnLaCompraDe3oMas',
                'MUG': 'NA'
            })
            test('Deberia responder con 5', () => {
                co.scan('VOUCHER')
                co.scan('VOUCHER')
                expect(co.total).toBe(5)
            })
            test('Deberia responder con 100', () => {
                co.scan('TSHIRT')
                co.scan('TSHIRT')
                co.scan('TSHIRT')
                co.scan('TSHIRT')
                co.scan('TSHIRT')
                expect(co.total).toBe(100)
            })
            test('Deberia responder con 122.5', () => {
                co.scan('MUG')
                co.scan('MUG')
                co.scan('MUG')
                expect(co.total).toBe(122.5)
            })
        })
    })
})