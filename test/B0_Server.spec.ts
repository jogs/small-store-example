import Server from '../src/server'
import request = require('supertest')

describe('API:', () => {
  describe('Validación:', () => {
    test('Debería responder con 201 a una petición POST, total 32.5', async () => {
      const response = await request(Server).post('/').send({productos: [
        'VOUCHER',
        'TSHIRT',
        'MUG'
      ]})
      expect(response.status).toBe(201)
      expect(response.body.total).toBe(32.5)
    })
  
    test('Debería responder con 201 a una petición POST, total 25', async () => {
      const response = await request(Server).post('/').send({productos: [
        'VOUCHER',
        'TSHIRT',
        'VOUCHER'
      ]})
      expect(response.status).toBe(201)
      expect(response.body.total).toBe(25)
    })
  
    test('Debería responder con 201 a una petición POST, total 81', async () => {
      const response = await request(Server).post('/').send({productos: [
        'TSHIRT',
        'TSHIRT',
        'TSHIRT',
        'VOUCHER',
        'TSHIRT'
      ]})
      expect(response.status).toBe(201)
      expect(response.body.total).toBe(81)
    })
  
    test('Debería responder con 201 a una petición POST, total 74.5', async () => {
      const response = await request(Server).post('/').send({productos: [
        'VOUCHER',
        'TSHIRT',
        'VOUCHER',
        'VOUCHER',
        'MUG',
        'TSHIRT',
        'TSHIRT'
      ]})
      expect(response.status).toBe(201)
      expect(response.body.total).toBe(74.5)
    })

    test('Debería responder con 201 a una petición POST, total 74.5', async () => {
      const response = await request(Server).post('/').send({
        productos: [
          'DESCONOCIDO',
          'VOUCHER',
          'TSHIRT',
          'VOUCHER',
          'VOUCHER',
          'MUG',
          'TSHIRT',
          'TSHIRT',
          'OTRO'
        ]
      })
      expect(response.status).toBe(201)
      expect(response.body.total).toBe(74.5)
    })
  })
  describe('Manejo de Errores:', () => {
    test('Debería responer con 404 a una petición GET', async () => {
      const response = await request(Server).get('/')
      expect(response.status).toBe(404)
      expect(response.body.message).toBe('Not Found')
    })
    test('Debería responer con 415 a una petición POST', async () => {
      const response = await request(Server).post('/').send('VOUCHER')
      expect(response.status).toBe(415)
      expect(response.body.message).toBe('Media Type no soportado: se esperaba un JSON con el atributo productos (string[])')
    })
    test('Debería responer con 415 a una petición POST', async () => {
      const response = await request(Server).post('/').send({productos: 'VOUCHER'})
      expect(response.status).toBe(400)
      expect(response.body.message).toBe('Petición mal formada: se esperaba un JSON con el atributo productos (string[])')
    })
  })
})