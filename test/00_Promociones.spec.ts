import { DosPorUno, DescuentoEnLaCompraDeN } from '../src/helpers/Promocion';

describe('Promociones:', () => {
  
  describe('Dos por Uno:', () => {

    test('Debería responder 3 al recibir 5', () => {
      expect(DosPorUno(5, 10)).toBe(30)
    })

    test('Debería responder 3 al recibir 6', () => {
      expect(DosPorUno(6, 10)).toBe(30)
    })

  })

  describe('Descuento en la compra de 3 o más:', () => {

    test('Debería responder 40 al recibir 2 productos de $20', () => {
      expect(DescuentoEnLaCompraDeN(3, 1, 2, 20)).toBe(40)
    })

    test('Debería responder 95 al recibir 5 productos de $20', () => {
      expect(DescuentoEnLaCompraDeN(3, 1, 5, 20)).toBe(95)
    })

  })
})
