import Productos from '../config/Productos'
import Product from './Product'
import PromoRules from './PromoRules';

/**
 *
 * La clase Checkout permite gestionar la adquicición de productos
 * @export
 * @class Checkout
 */
export default class Checkout {
    private _productos: object = Productos
    private _stack: object = {}
    private _promoRules: PromoRules
    private _descuento: number = 0
    
    /**
     *Creates an instance of Checkout.
     * @param {object} rules
     * @memberof Checkout
     */
    constructor(rules: object = {}) {
        this._promoRules = new PromoRules(rules)
    }

    /**
     *
     * El método scan recibe un codigo de producto, si se encuentra, se añade el registro o se incrementa
     * en uno el regitro de este
     * @param {string} code
     * @memberof Checkout
     */
    public scan (code: string): void {
        if (this._productos.hasOwnProperty(code)) {
            this._append(code)
        }
    }

    /**
     *
     * El método _append permite a gestiona la instancia de un producto o el incremento del contados de este
     * @private
     * @param {string} code
     * @memberof Checkout
     */
    private _append (code: string): void {
        if (this._stack.hasOwnProperty(code)) {
            this._find(code).inc()
        } else {
            (this as any)._stack[code] = new Product(
                code,
                (this as any)._productos[code].nombre,
                (this as any)._productos[code].precio
            )
        }
    }

    /**
     *
     * El método _find permite acceder a un producto en el _stack solo con su codigo de este
     * @private
     * @param {string} code
     * @returns {*}
     * @memberof Checkout
     */
    private _find (code: string): any {
        return (this as any)._stack[code]
    }

    /**
     *
     * entrega la información de un producto instancia de la Clase Product en formato JSON
     * @private
     * @param {string} code
     * @returns {*}
     * @memberof Checkout
     */
    private _findJSON (code: string): any {
        return this._find(code).toJSON()
    }

    /**
     *
     * Permite buscar la información registrada de un producto, ingresando su codigo
     * @param {string} code
     * @returns {object}
     * @memberof Checkout
     */
    public product (code: string): object {
        return this._promoRules.applyPromo(this._findJSON(code))
    }

    /**
     * El getter products entrega un arraglo de datos, que describe la información
     * de cada producto registrado agrupandolos por cantidad
     * @readonly
     * @type {object[]}
     * @memberof Checkout
     */
    get products (): object[] {
        return this._codes.map(code => this.product(code))
    }

    /**
     *
     * El getter _codes indica el conjunto de codigos ingresados hasta el momento
     * @readonly
     * @type {string[]}
     * @memberof Checkout
     */
    get _codes (): string[] {
        return Object.keys(this._stack)
    }

    /**
     *
     * El getter subtotal permite entrega la suma de precio de cada producto ingresado
     * hasta el momento de la invocación
     * @readonly
     * @type {number}
     * @memberof Checkout
     */
    get subtotal (): number {
        return this._reduceProductsTo('total')
    }

    /**
     *
     * El getter descuento permite entrega la suma del desuento aplicado de cada producto ingresado
     * hasta el momento de la invocación
     * @readonly
     * @type {number}
     * @memberof Checkout
     */
    get descuento (): number {
        return this._reduceProductsTo('descuento')
    }

    /**
     *
     * El getter total permite entrega la diferencia de el subtotal y el descuento
     * @readonly
     * @type {number}
     * @memberof Checkout
     */
    get total (): number {
        return this._reduceProductsTo('totalPromo')
    }

    /**
     * Aplica la funcion reduce a la lista de productos, para calcular
     * la suma de un attributo dado de este objeto
     * @private
     * @param {string} key
     * @returns {number}
     * @memberof Checkout
     */
    private _reduceProductsTo (key: string): number {
        return this.products.reduce((acumulador, product) => {
            return acumulador + (product as any)[key]
        }, 0)
    }
}