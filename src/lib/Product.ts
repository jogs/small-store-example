/**
 *
 * La clase Product ayuda a controlar la adquición de un producto especifico
 * @export
 * @class Product
 */
export default class Product {
    public code: string
    public nombre: string
    public precio: number
    public cantidad: number = 1
    
    /**
     * Creates an instance of Product.
     * @param {string} code
     * @param {string} nombre
     * @param {number} precio
     * @memberof Product
     */
    constructor (code: string, nombre: string, precio: number) {
        this.code = code
        this.nombre = nombre
        this.precio = precio
    }

    /**
     *
     * El getter total, entrega el producto (aritmetico) de precio y cantidad
     * @readonly
     * @type {number}
     * @memberof Product
     */
    get total (): number {
        return this.precio * this.cantidad
    }

    /**
     *
     * El metodo inc, incrementa en 1 la cantidad de productos adquiridos
     * @memberof Product
     */
    public inc (): void {
        this.cantidad++
    }

    /**
     *
     * El método toJSON entrega una representación en formato JSON de este objeto
     * @returns {object}
     * @memberof Product
     */
    public toJSON (): object {
        return {
            code: this.code,
            nombre: this.nombre,
            precio: this.precio,
            cantidad: this.cantidad,
            total: this.total
        }
    }

    /**
     *
     * El método toString entrega una representación en formato String de este objeto
     * @returns {string}
     * @memberof Product
     */
    public toString (): string {
        return `${this.code}: ${this.nombre} | ${this.cantidad} x ${this.precio} = ${this.total}`
    }
}