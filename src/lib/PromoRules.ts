import PromoRule from './PromoRule'

/**
 * La clase PromoRules contiene el concentrado de todas las promociones vigentes
 * lo que permite aplicar una promoción a un conjunto de datos provisto por la
 * clase Checkout
 * @export
 * @class PromoRules
 */
export default class PromoRules {
    private _codes: string[] = []
    private _rules: object
    
    /**
     *Creates an instance of PromoRules.
     * @param {object} rules
     * @memberof PromoRules
     */
    constructor (rules: object) {
        this._codes = Object.keys(rules)
        this._rules = this._codes.reduce((obj, code) => {
            let tmpObj: any = {}
            tmpObj[code] = new PromoRule(code, (rules as any)[code])
            return Object.assign({}, obj, tmpObj)
        }, ({}))
    }

    /**
     * El método _totalPromo Invoca a la instancia correspondiente de la clase PromoRule
     * segun el codigo de producto que se le entrege
     * @param {string} code
     * @param {number} cantidad
     * @param {number} precioUnitario
     * @returns {number}
     * @memberof PromoRules
     */
    private _totalPromo (code: string, cantidad: number, precioUnitario: number): number {
        if (this._codes.includes(code)) {
            return (this as any)._rules[code].promo(cantidad, precioUnitario)
        } else return cantidad * precioUnitario
    }

    /**
     * El método applyPromo recibe un JSON desde la clase Checkout y aplica la promoción correspondiente
     * @param {object} data
     * @returns {object}
     * @memberof PromoRules
     */
    public applyPromo (data: object): object {
        let newData: any = Object.assign({}, data)
        let totalPromo = this._totalPromo(newData.code, newData.cantidad, newData.precio)
        newData['totalPromo'] = totalPromo
        newData['descuento'] = newData.total - totalPromo
        return newData
    }
}