import Promociones from '../helpers/Promocion'

/**
 * La clase PromoRule ayuda a asignar de forma dinamica la funcíon
 * que aplica una promoción deteterminada
 * @export
 * @class PromoRule
 */
export default class PromoRule {
    public code: string
    private _promo: string
    private _promos: object = Promociones

    /**
     *Creates an instance of PromoRule.
     * @param {string} code
     * @param {string} promo
     * @memberof PromoRule
     */
    constructor(code: string, promo: string) {
        this.code = code
        this._promo = promo
    }

    /**
     * El método promo selecciona dinamicamente la función que aplica la promoción
     * y la ejecuta con los parametros por defecto
     * @param {number} cantidad
     * @param {number} precioUnitario
     * @returns {number}
     * @memberof PromoRule
     */
    public promo(cantidad: number, precioUnitario: number): number {
        if (this._promos.hasOwnProperty(this.promoName)) {
            return (this as any)._promos[this.promoName](cantidad, precioUnitario)
        } else return cantidad * precioUnitario
    }

    /**
     *
     * Getter para saber el nombre de la promocuón que se esta aplicando
     * @readonly
     * @type {string}
     * @memberof PromoRule
     */
    get promoName(): string {
        return this._promo
    }
}