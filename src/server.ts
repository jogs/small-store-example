import express from 'express'
import cors from 'cors'
import helmet from 'helmet'
import {json} from 'body-parser'
import Checkout from './lib/Checkout'
import Promociones from './config/Promociones'
const server = express()

server.use(cors())
server.use(helmet())
server.use(json())

server.route('/')
    .post((req, res) => {
        if (req.is('json')) {
            if (Array.isArray(req.body.productos)) {
                let co = new Checkout(Promociones)

                req.body.productos.forEach((code: string) => co.scan(code))

                res.status(201).json({
                    productos: co.products,
                    subtotal: co.subtotal,
                    descuento: co.descuento,
                    total: co.total
                })
            } else {
                res.status(400).json({ message: 'Petición mal formada: se esperaba un JSON con el atributo productos (string[])' })
            }
        } else {
            res.status(415).json({ message: 'Media Type no soportado: se esperaba un JSON con el atributo productos (string[])'})
        }
    })

server.use((req, res, next) => {
    res.status(404).json({message: 'Not Found'})
})

server.use((err: any, req: any, res: { status: (arg0: number) => { json: (arg0: { message: string; }) => void; }; }, next: any) => {
    console.error(err.stack)
    res.status(500).json({message: 'Internal Server Error'})
})

export default server
