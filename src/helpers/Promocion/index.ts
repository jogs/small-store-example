import DosPorUno from './DosPorUno';
import DescuentoEnLaCompraDeN from './DescuentoEnLaCompraDeN';

export {
  DosPorUno,
  DescuentoEnLaCompraDeN
}

export default{
  DosPorUno,
  DescuentoEnLaCompraDe3oMas (cantidad: number, precioUnitario: number): number {
    return DescuentoEnLaCompraDeN(3, 1, cantidad, precioUnitario)
  }
}