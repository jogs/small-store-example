/**
 * Esta función indica el monto a cobrar despues de aplicar la promoción
 * En la compra de 3 o mas productos se decuenta X por cada producto
 * @export
 * @param {number} cantidadMinima - Cantidad minima de produtos a adquirir para que se aplique el descuento
 * @param {number} descuentoPorPieza - Descuento que se aplica por cada producto adquirido
 * @param {number} cantidadActual - Cantidad de productos
 * @param {number} precioPorPieza - Precio unitario del producto
 * @returns {number} Precio despues del descuento
 */
export default function DescuentoEnLaCompraDeN (cantidadMinima: number, descuentoPorPieza: number,  cantidadActual: number, precioPorPieza: number): number {
  if (cantidadMinima > cantidadActual) {
    return cantidadActual * precioPorPieza
  } else {
    return cantidadActual * (precioPorPieza - descuentoPorPieza)
  }
}