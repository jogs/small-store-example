/**
 * Esta función indica el monto a cobrar despues de aplicar la promoción
 * En la compra de cada 2 productos se decuenta el precio de uno
 * @export
 * @param {number} cantidadActual  - Cantidad de productos antes de aplicar promoción
 * @param {number} precioUnitario -  Precion por pieza
 * @returns {number} Cantidad de productos a cobrar aplicando la promoción
 */
export default function DosPorUno (cantidadActual: number, precioUnitario: number): number {
  let residuo: number = cantidadActual % 2
  let cantidadPromocion: number
  if (residuo === 1) {
    cantidadPromocion = ((cantidadActual - 1) / 2) + 1
  } else {
    cantidadPromocion = cantidadActual / 2
  }
  return cantidadPromocion * precioUnitario
}