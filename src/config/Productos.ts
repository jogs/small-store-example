export default {
  VOUCHER: {
    nombre: 'Peppa pig voucher',
    precio: 5
  },
  TSHIRT: {
    nombre: 'Avengers end game t-shirt',
    precio: 20
  },
  MUG: {
    nombre: 'Salad mug',
    precio: 7.5
  }
}