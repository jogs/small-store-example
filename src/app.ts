import Server from './server'

const PORT: number = 3000

Server.listen(PORT, () => console.log(`Aplicación corriendo en el puerto ${PORT}`))