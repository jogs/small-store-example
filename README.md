# Small Store Example _v1.0.0_

## Dependencias

* Node.js: v10.15.3
* Docker: 18.09.2 (opcional)

## Node.js (ejecución local)

### Ejecución

```bash
npm install && npm run test

npm run build:all

node dist/app.js
```

## Docker

### Build

* Developmet

```bash
docker build -t small-store:dev -f build/development.Dockerfile .
```

* Production

```bash
docker build -t small-store:prod -f build/production.Dockerfile .
```

### Deploy

* Development

```bash
docker run --name small-store-dev -d -p 3000:3000 -v [project_path]src:/usr/src/src small-store:dev
```

* Production

```bash
docker run --name small-store-prod -d -p 3001:3000 small-store:prod
```

## Sobre el proyecto

Esta es una pequeña aplicación con la función de un punto de venta, actualmente soporta solo 3 productos y 2 ofertas (promociones) aplicables a cualquiera de los mismos, tanto la lista de productos como las ofertas son extensibles para aumentar la capacidad de la aplicación

### Estructura de carperas

* `src` / `dist`
  + `config` (contiene los archivos con la lista de productos, y las promociones asociadas a estos)
    - `Productos`
    - `Promociones` (se indica el nombre del producto y el nombre de la función a aplicar, esta se localiza en *helpers/Promociones/index*)
  + `helpers`
    - `Promociones` (la carpeta contiene las funciones que aplican las promociones a un producto)
  + `lib` (contiene las clases necesarias para que el punto de venta funcione, *la clase principal es Checkout*)
    - `Checkout`: esta clase recibe el objeto promociones de la carpeta config
    - `Product`: esta clase controla el conteo de escaneos y subtotal por producto
    - `PromoRule`: esta clase contiene la relación 1:1 de producto:promocion, y realiza la ejecuión
    - `PromoRules`: esta clase contiene tantas PromoRule como registros en el objeto config/Promociones
  + `app`: punto de entrada de la aplicación (api)
  + `server`: contiene todas las configuraciones relacionadas al api

### Uso de la clase Checkout

La clase Checkout calcula el precio a pagar por una cantidad dada de productos

```javascript
let co = new Checkout({
    'VOUCHER': 'DosPorUno',
    'TSHIRT': 'DescuentoEnLaCompraDe3oMas',
    'MUG': 'NA'
})

co.scan('VOUCHER')
co.scan('VOUCHER')
co.scan('TSHIRT')
co.scan('TSHIRT')
co.scan('TSHIRT')
co.scan('TSHIRT')
co.scan('TSHIRT')
co.scan('MUG')
co.scan('MUG')
co.scan('MUG')

console.log(co.subtotal)
console.log(co.descuento)
console.log(con.total)
```

### Uso del API

De forma adicional se expone un api muy simple para poder consumir el servicio

La interfaz es simple, se debe enviar una petición de tipo POST a la URI /
con un JSON con un atributo productos el cual debe ser un arreglo de strings

El arreglo productos debe contener los codigos de los productos, en caso de recibir
un codigo no registrado en el sistema, este simplemente se ignora

* Petición

```
# autogenerado por postman

POST  HTTP/1.1
Host: 127.0.0.1:3001
Content-Type: application/json
cache-control: no-cache
Postman-Token: 0241b129-b58a-47e1-a91a-788b7f230f35

{"productos": ["VOUCHER", "VOUCHER", "MUG"]}
```

* Respuesta

```json
{
    "productos": [
        {
            "code": "VOUCHER",
            "nombre": "Peppa pig voucher",
            "precio": 5,
            "cantidad": 2,
            "total": 10,
            "totalPromo": 5,
            "descuento": 5
        },
        {
            "code": "MUG",
            "nombre": "Salad mug",
            "precio": 7.5,
            "cantidad": 1,
            "total": 7.5,
            "totalPromo": 7.5,
            "descuento": 0
        }
    ],
    "subtotal": 17.5,
    "descuento": 5,
    "total": 12.5
}
```

## Sobre las pruebas

La suite de pruebas evalua la mayor parte del código exceptuando el endpoint (app.js)

Las pruebas se dividen en tres niveles, cada archivo comienza con alguno de estos prefijos

* `0X`: funciones
* `AX`: clases
* `BX`: implementación

Al ejecutar las pruebas, se genera la carpeta *report* dentro de *test* (test/report)

* `test/report/index.html`: describe el resultado de las pruebas
* `test/report/coverage/Icov-report/index.html`: muestra la covertura de las pruebas al código que se evaluo

Las siguientes pruebas se realizan a nivel de implementación (`B0_Server.spce.ts`)

| Items | Total |
|-------|-------|
| VOUCHER, TSHIRT, MUG | 32.50 |
| VOUCHER, TSHIRT, VOUCHER | 25 |
| TSHIRT, TSHIRT, TSHIRT, VOUCHER, TSHIRT | 81 |
| VOUCHER, TSHIRT, VOUCHER, VOUCHER, MUG, TSHIRT, TSHIRT | 74.5 |