FROM node:10.15.3-alpine as builder

COPY ["package.json", "package-lock.json", "/usr/src/"]

WORKDIR /usr/src

RUN npm install --only=production

COPY [".", "/usr/src/"]

RUN npm install --only=development

RUN npm run test

RUN npm run build:js


# Productive image
FROM node:10.15.3-alpine

COPY ["package.json", "package-lock.json", "/usr/src/"]

WORKDIR /usr/src

RUN npm install --only=production

COPY --from=builder ["/usr/src/dist", "/usr/src/"]

EXPOSE 3000

CMD ["node", "app.js"]